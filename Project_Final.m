%%%%%%%%%%%%%%%%%%%%****  Final Project  ****%%%%%%%%%%%%%%%%%%%%%%%%%
%
%	File Name:	   Project_Final.m
%	Author(s):     Ramsundar K G, Suresh G, Muhilan R
%	Date:		   08 Dec 2015
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear;
close all;
warning('off','all')
addpath(genpath(pwd));

%%%
%%% Load the dataset
%%% Dataset #1
load( './training data/Sampledata_1.mat' );     Spikes_Train_1 = spikes;
load( './test data/Sampledata_test_1.mat' );    Spikes_Test_1 = spikes;
%%% Dataset #2
load( './training data/Sampledata_2.mat' );     Spikes_Train_2 = spikes;
load( './test data/Sampledata_test_2.mat' );    Spikes_Test_2 = spikes;
%%% Dataset #3
load( './training data/Sampledata_3.mat' );     Spikes_Train_3 = spikes;
load( './test data/Sampledata_test_3.mat' );    Spikes_Test_3 = spikes;
%%% Dataset #4
load( './training data/Sampledata_4.mat' );     Spikes_Train_4 = spikes;
load( './test data/Sampledata_test_4.mat' );    Spikes_Test_4 = spikes;

%load( '../../test data/Sampledata_test_3.mat' );

figure(1); % Dataset # 1 Spike Plot
figure(2); % Dataset # 2 Spike Plot
figure(3); % Dataset # 3 Spike Plot
figure(4); % Dataset # 4 Spike Plot
figure(5); % PCA Space clustering plots

%%%%%%%%%%%%%%%%%%%%% Model 1: Dataset 1 %%%%%%%%%%%%%%%%%%%%%
%%%
%%% Design Parameters
clearvars -except Spikes_Train_1 Spikes_Train_2 ...
                  Spikes_Train_3 Spikes_Train_4 ...
                  Spikes_Test_1 Spikes_Test_2 ...
                  Spikes_Test_3 Spikes_Test_4
WAV_LEVEL = 4;
FACTOR_AMP = 5;
OUTLIER_SD = 2;
OL_DOUBLE_THRESH = 0.30;
DS_Num = 1;
RESCALE_SIZE_X = 100;
PCA_DIM = 3;

%%%
%%% Spike snipets
LenSnips = size(Spikes_Train_1, 2);
NumSnips = size(Spikes_Train_1, 1);

fprintf('\nDataset #1: Training...\n');
%%%%%%%%%%%% Training Dataset #1 %%%%%%%%%%%%
%%%
%%% 1. Pre-Processing
%%% Remove outliers
Outliers_bound = mean(Spikes_Train_1) + abs(OUTLIER_SD .* std(Spikes_Train_1));
Outliers_Mask = zeros(NumSnips, 1);
for i=1:size(Spikes_Train_1,1)
    comparison = Spikes_Train_1(i,:) >= Outliers_bound;
    if ( sum(comparison) > 0)
        Outliers_Mask(i,:) = 1;
    end
end
Spikes_No_Outliers = Spikes_Train_1(~Outliers_Mask, :);

%%%
%%% Remove Double Spikes
X = Spikes_Train_1;
X_Sm = X;
for i = 1:size(X,1)
    % Find the peaks
    peak = [];
    [peak(1,:), peak(2,:)] = findpeaks(X(i,:));
    peak = [peak [X(i,1) X(i,end); 1 size(X,2)]];
    [~,sort_ind] = sort(peak(1,:), 'descend');
    peak = peak(:, sort_ind);
    
    % Check for couble Spike_Snips_1
    if(length(peak) > 1)
        ratio = abs(peak(1,2)/peak(1,1));
        peak = peak(:, peak(2,:)<10 | peak(2,:)>22);
        if(ratio > OL_DOUBLE_THRESH)
            inv_peak = [];
            % Find the two minima
            Xinv = 1.01*max(X(i,:)) - X(i,:);
            [inv_peak(1,:), inv_peak(2,:)] = findpeaks(Xinv);
            R_min = inv_peak(2,find(inv_peak(2,:)>peak(2,1), 1));
            L_min = inv_peak(2,find(inv_peak(2,:)<peak(2,1), 1, 'last'));
            if(isempty(L_min)), L_min = 1; end
            if(isempty(R_min)), R_min = size(X,2); end
            
            % Interpolate and supress the double Spike_Snips_1
            a = X(i,L_min);
            b = X(i,R_min);
            if(a < b)
            Sm = a:(abs(b-a)/abs(R_min-L_min)):b;
            else
            Sm = a:-(abs(b-a)/abs(R_min-L_min)):b;
            end
            X_Sm(i,L_min:R_min) = Sm;
        end
    end
end

%%% Amplification using double gaussian window
g_vect_1 = normpdf(1:48, 13, 4);
g_vect_1 = g_vect_1 * ((FACTOR_AMP-1)/max(g_vect_1));
g_vect_2 = normpdf(1:48, 25, 4);
g_vect_2 = g_vect_2 * ((FACTOR_AMP-1)/max(g_vect_2));
Amp_Fact = 1 + g_vect_1 + g_vect_2;
X_amp = (repmat(Amp_Fact, size(X_Sm,1), 1) .* X_Sm);

%%%
%%% 2. Feature Extraction
%%% Wavelet transform
Wavelt_Input = X_amp;
Wavelt_range = 1:size(Wavelt_Input,2);
Wavelet_Coeff = zeros(size(Wavelt_Input,1), length(Wavelt_range));
for i = 1:size(Wavelt_Input,1)
    %Wavelet_Coeff(i,:) = wavedec(Spike_Snips(i,range), WAV_LEVEL, 'haar');
    Wavelet_Coeff(i,:) = mdwt(Wavelt_Input(i,Wavelt_range), daubcqf(4,'miv'), WAV_LEVEL);
end

%%%
%%% PCA
PCA_feat_range = 24:48; % Use only the high frequency band
PCA_in = Wavelet_Coeff(~Outliers_Mask, PCA_feat_range);
[PCA_Coeff, PCA_Score, PCA_latent, PCA_t, PCS_e, PCA_mu] = pca( PCA_in );

X_PCA = PCA_Score(:,1:3);
X_all = Wavelet_Coeff(:,PCA_feat_range);
X_PCA_Train =  X_all - repmat(mean(X_all), [size(X_all,1) 1]);
X_PCA_Train = X_PCA_Train * PCA_Coeff;

%%%
%%% 4. Clustering
%%% Find the Number of clusters using Vision based technique
nk = nchoosek(1:PCA_DIM,2);
NumClus_vi = [];
for vi = 1:PCA_DIM
    fig = figure(12);
    set(fig, 'Visible', 'off');
    scatter(PCA_Score(:,nk(vi,1)), PCA_Score(:,nk(vi,2)));
    F = getframe(fig);
    close(fig);
    
    img_rgb = F.cdata;
    RESCALE_SIZE_Y = size(img_rgb,2)/size(img_rgb,1)*RESCALE_SIZE_X;
    img_rgb = imresize(img_rgb, [RESCALE_SIZE_X RESCALE_SIZE_Y]);
    img_gray = rgb2gray(img_rgb);
    img_gray = imfilter(img_gray, fspecial('gaussian',[5 5], 2), 'replicate');
    img_bin = ~im2bw(img_gray, graythresh(img_gray));
    img_bin = imopen(img_bin, strel('disk', 2));
    img_stat = regionprops(img_bin, 'Centroid');
    NumClus_vi = [NumClus_vi, size(img_stat, 1);];
end
k = max(NumClus_vi);
fprintf('Number of Clusters Found: %d\n', k);

%%% Hierarchical clustering
DIST = 'cityblock';
%DIST = 'mahalanobis';
Y_Pure = clusterdata(X_PCA, 'linkage', 'ward', 'maxclust', k,...
                    'distance', DIST);
X_Pure = X_PCA;

%%%
%%% 5. Fit outliers to clusters
KNN_Model = fitcknn(X_Pure, Y_Pure, 'NumNeighbors', 7);
Y_Train = predict(KNN_Model, X_PCA_Train(:,1:3));


fprintf('Dataset #1: Testing...\n');
%%%%%%%%%%%% Testing Dataset #1 %%%%%%%%%%%%
%%%
%%% 1. Pre-Processing
%%% Remove outliers
Outliers_bound = mean(Spikes_Test_1) + abs(OUTLIER_SD .* std(Spikes_Test_1));
Outliers_Mask = zeros(NumSnips, 1);
for i=1:size(Spikes_Test_1,1)
    comparison = Spikes_Test_1(i,:) >= Outliers_bound;
    if ( sum(comparison) > 0)
        Outliers_Mask(i,:) = 1;
    end
end
Spikes_No_Outliers = Spikes_Test_1(~Outliers_Mask, :);

%%%
%%% Remove Double Spikes
X = Spikes_Test_1;
X_Sm = X;
for i = 1:size(X,1)
    % Find the peaks
    peak = [];
    [peak(1,:), peak(2,:)] = findpeaks(X(i,:));
    peak = [peak [X(i,1) X(i,end); 1 size(X,2)]];
    [~,sort_ind] = sort(peak(1,:), 'descend');
    peak = peak(:, sort_ind);
    
    % Check for couble Spike_Snips_1
    if(length(peak) > 1)
        ratio = abs(peak(1,2)/peak(1,1));
        peak = peak(:, peak(2,:)<10 | peak(2,:)>22);
        if(ratio > OL_DOUBLE_THRESH)
            inv_peak = [];
            % Find the two minima
            Xinv = 1.01*max(X(i,:)) - X(i,:);
            [inv_peak(1,:), inv_peak(2,:)] = findpeaks(Xinv);
            R_min = inv_peak(2,find(inv_peak(2,:)>peak(2,1), 1));
            L_min = inv_peak(2,find(inv_peak(2,:)<peak(2,1), 1, 'last'));
            if(isempty(L_min)), L_min = 1; end
            if(isempty(R_min)), R_min = size(X,2); end
            
            % Interpolate and supress the double Spike_Snips_1
            a = X(i,L_min);
            b = X(i,R_min);
            if(a < b)
            Sm = a:(abs(b-a)/abs(R_min-L_min)):b;
            else
            Sm = a:-(abs(b-a)/abs(R_min-L_min)):b;
            end
            X_Sm(i,L_min:R_min) = Sm;
        end
    end
end

%%% Amplification using double gaussian window
g_vect_1 = normpdf(1:48, 13, 4);
g_vect_1 = g_vect_1 * ((FACTOR_AMP-1)/max(g_vect_1));
g_vect_2 = normpdf(1:48, 25, 4);
g_vect_2 = g_vect_2 * ((FACTOR_AMP-1)/max(g_vect_2));
Amp_Fact = 1 + g_vect_1 + g_vect_2;
X_amp = (repmat(Amp_Fact, size(X_Sm,1), 1) .* X_Sm);

%%%
%%% 2. Feature Extraction
%%% Wavelet transform
Wavelt_Input = X_amp;
Wavelt_range = 1:size(Wavelt_Input,2);
Wavelet_Coeff = zeros(size(Wavelt_Input,1), length(Wavelt_range));
for i = 1:size(Wavelt_Input,1)
    %Wavelet_Coeff(i,:) = wavedec(Spike_Snips(i,range), WAV_LEVEL, 'haar');
    Wavelet_Coeff(i,:) = mdwt(Wavelt_Input(i,Wavelt_range), daubcqf(4,'miv'), WAV_LEVEL);
end

%%%
%%% PCA
PCA_feat_range = 24:48; % Use only the high frequency band
PCA_in = Wavelet_Coeff(~Outliers_Mask, PCA_feat_range);
[PCA_Coeff, PCA_Score, PCA_latent, PCA_t, PCS_e, PCA_mu] = pca( PCA_in );

X_PCA = PCA_Score(:,1:3);
X_all = Wavelet_Coeff(:,PCA_feat_range);
X_PCA_Test =  X_all - repmat(mean(X_all), [size(X_all,1) 1]);
X_PCA_Test = X_PCA_Test * PCA_Coeff;

%%%
%%% 4. Clustering
%%% Hierarchical clustering
DIST = 'cityblock';
%DIST = 'mahalanobis';
Y_Pure = clusterdata(X_PCA, 'linkage', 'ward', 'maxclust', k,...
                    'distance', DIST);
X_Pure = X_PCA;

%%%
%%% 5. Fit outliers to clusters
KNN_Model = fitcknn(X_Pure, Y_Pure, 'NumNeighbors', 7);
Y_Test = predict(KNN_Model, X_PCA_Test(:,1:3));


%%%%%%%%%%%% Ploting of Results %%%%%%%%%%%%
%%%
PlotColor =      ['r', 'g', 'b', 'm', 'k', 'c', 'y'];
PlotColor_mean = ['k', 'k', 'y', 'k', 'w', 'k', 'k'];
leg_text = cell(k,1);
%%% Plot Test Results Alone (Training Results are not plotted)
for i = 1:k
    C = Spikes_Test_1((Y_Test==i), :);
    figure(1); subplot(2,2,i);
    plot(C', PlotColor(i));
    hold on;
    plot(mean(C), PlotColor_mean(i));
    title(['Test Spike Cluster # ' num2str(i)]);
    subplot(2,2,4);
    plot(C', PlotColor(i));
    hold on;
    figure(5); subplot(2,2,DS_Num);
    C = X_PCA_Test((Y_Test==i), :);
    scatter3(C(:,1), C(:,2), C(:,3), PlotColor(i));
    hold on;
end
figure(1);
subplot(2,2,4);
title('Overlaped Clustered Test Spikes');
figure(5);
subplot(2,2,DS_Num);
title('Test Dataset #1: PCA Space');
xlabel('Wavelet Coeff 1');
ylabel('Wavelet Coeff 2');
zlabel('Wavelet Coeff 3');
hold off;

xlswrite('Labels_Sampledata_test_1.xlsx', Y_Test);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%% Model 2: Dataset 2 %%%%%%%%%%%%%%%%%%%%%
%%%
%%% Design Parameters
clearvars -except Spikes_Train_1 Spikes_Train_2 ...
                  Spikes_Train_3 Spikes_Train_4 ...
                  Spikes_Test_1 Spikes_Test_2 ...
                  Spikes_Test_3 Spikes_Test_4
WAV_LEVEL = 4;
FACTOR_AMP = 5;
OUTLIER_SD = 2;
OL_DOUBLE_THRESH = 0.30;
DS_Num = 2;
RESCALE_SIZE_X = 100;
PCA_DIM = 3;

%%%
%%% Spike snipets
LenSnips = size(Spikes_Train_2, 2);
NumSnips = size(Spikes_Train_2, 1);

fprintf('\nDataset #2: Training...\n');
%%%%%%%%%%%% Training Dataset #2 %%%%%%%%%%%%
%%%
%%% 1. Pre-Processing
%%% Remove outliers
Outliers_bound = mean(Spikes_Train_2) + abs(OUTLIER_SD .* std(Spikes_Train_2));
Outliers_Mask = zeros(NumSnips, 1);
for i=1:size(Spikes_Train_2,1)
    comparison = Spikes_Train_2(i,:) >= Outliers_bound;
    if ( sum(comparison) > 0)
        Outliers_Mask(i,:) = 1;
    end
end
Spikes_No_Outliers = Spikes_Train_2(~Outliers_Mask, :);

%%%
%%% Remove Double Spikes
X = Spikes_Train_2;
X_Sm = X;
for i = 1:size(X,1)
    % Find the peaks
    peak = [];
    [peak(1,:), peak(2,:)] = findpeaks(X(i,:));
    peak = [peak [X(i,1) X(i,end); 1 size(X,2)]];
    [~,sort_ind] = sort(peak(1,:), 'descend');
    peak = peak(:, sort_ind);
    
    % Check for couble Spike_Snips_1
    if(length(peak) > 1)
        ratio = abs(peak(1,2)/peak(1,1));
        peak = peak(:, peak(2,:)<10 | peak(2,:)>22);
        if(ratio > OL_DOUBLE_THRESH)
            inv_peak = [];
            % Find the two minima
            Xinv = 1.01*max(X(i,:)) - X(i,:);
            [inv_peak(1,:), inv_peak(2,:)] = findpeaks(Xinv);
            R_min = inv_peak(2,find(inv_peak(2,:)>peak(2,1), 1));
            L_min = inv_peak(2,find(inv_peak(2,:)<peak(2,1), 1, 'last'));
            if(isempty(L_min)), L_min = 1; end
            if(isempty(R_min)), R_min = size(X,2); end
            
            % Interpolate and supress the double Spike_Snips_1
            a = X(i,L_min);
            b = X(i,R_min);
            if(a < b)
            Sm = a:(abs(b-a)/abs(R_min-L_min)):b;
            else
            Sm = a:-(abs(b-a)/abs(R_min-L_min)):b;
            end
            X_Sm(i,L_min:R_min) = Sm;
        end
    end
end

%%% Amplification using double gaussian window
g_vect_1 = normpdf(1:48, 13, 4);
g_vect_1 = g_vect_1 * ((FACTOR_AMP-1)/max(g_vect_1));
g_vect_2 = normpdf(1:48, 25, 4);
g_vect_2 = g_vect_2 * ((FACTOR_AMP-1)/max(g_vect_2));
Amp_Fact = 1 + g_vect_1 + g_vect_2;
X_amp = (repmat(Amp_Fact, size(X_Sm,1), 1) .* X_Sm);

%%%
%%% 2. Feature Extraction
%%% Wavelet transform
Wavelt_Input = X_amp;
Wavelt_range = 1:size(Wavelt_Input,2);
Wavelet_Coeff = zeros(size(Wavelt_Input,1), length(Wavelt_range));
for i = 1:size(Wavelt_Input,1)
    %Wavelet_Coeff(i,:) = wavedec(Spike_Snips(i,range), WAV_LEVEL, 'haar');
    Wavelet_Coeff(i,:) = mdwt(Wavelt_Input(i,Wavelt_range), daubcqf(4,'miv'), WAV_LEVEL);
end

%%%
%%% PCA
PCA_feat_range = 24:48; % Use only the high frequency band
PCA_in = Wavelet_Coeff(~Outliers_Mask, PCA_feat_range);
[PCA_Coeff, PCA_Score, PCA_latent, PCA_t, PCS_e, PCA_mu] = pca( PCA_in );

X_PCA = PCA_Score(:,1:3);
X_all = Wavelet_Coeff(:,PCA_feat_range);
X_PCA_Train =  X_all - repmat(mean(X_all), [size(X_all,1) 1]);
X_PCA_Train = X_PCA_Train * PCA_Coeff;

%%%
%%% 4. Clustering
%%% Find the Number of clusters using Vision based technique
nk = nchoosek(1:PCA_DIM,2);
NumClus_vi = [];
for vi = 1:PCA_DIM
    fig = figure(12);
    set(fig, 'Visible', 'off');
    scatter(PCA_Score(:,nk(vi,1)), PCA_Score(:,nk(vi,2)));
    F = getframe(fig);
    close(fig);
    
    img_rgb = F.cdata;
    RESCALE_SIZE_Y = size(img_rgb,2)/size(img_rgb,1)*RESCALE_SIZE_X;
    img_rgb = imresize(img_rgb, [RESCALE_SIZE_X RESCALE_SIZE_Y]);
    img_gray = rgb2gray(img_rgb);
    img_gray = imfilter(img_gray, fspecial('gaussian',[5 5], 2), 'replicate');
    img_bin = ~im2bw(img_gray, graythresh(img_gray));
    img_bin = imopen(img_bin, strel('disk', 2));
    img_stat = regionprops(img_bin, 'Centroid');
    NumClus_vi = [NumClus_vi, size(img_stat, 1);];
end
k = max(NumClus_vi);
fprintf('Number of Clusters Found: %d\n', k);

%%% Hierarchical clustering
DIST = 'cityblock';
%DIST = 'mahalanobis';
Y_Pure = clusterdata(X_PCA, 'linkage', 'ward', 'maxclust', k,...
                    'distance', DIST);
X_Pure = X_PCA;

%%%
%%% 5. Fit outliers to clusters
KNN_Model = fitcknn(X_Pure, Y_Pure, 'NumNeighbors', 7);
Y_Train = predict(KNN_Model, X_PCA_Train(:,1:3));


fprintf('Dataset #2: Testing...\n');
%%%%%%%%%%%% Testing Dataset #2 %%%%%%%%%%%%
%%%
%%% 1. Pre-Processing
%%% Remove outliers
Outliers_bound = mean(Spikes_Test_2) + abs(OUTLIER_SD .* std(Spikes_Test_2));
Outliers_Mask = zeros(NumSnips, 1);
for i=1:size(Spikes_Test_2,1)
    comparison = Spikes_Test_2(i,:) >= Outliers_bound;
    if ( sum(comparison) > 0)
        Outliers_Mask(i,:) = 1;
    end
end
Spikes_No_Outliers = Spikes_Test_2(~Outliers_Mask, :);

%%%
%%% Remove Double Spikes
X = Spikes_Test_2;
X_Sm = X;
for i = 1:size(X,1)
    % Find the peaks
    peak = [];
    [peak(1,:), peak(2,:)] = findpeaks(X(i,:));
    peak = [peak [X(i,1) X(i,end); 1 size(X,2)]];
    [~,sort_ind] = sort(peak(1,:), 'descend');
    peak = peak(:, sort_ind);
    
    % Check for couble Spike_Snips_1
    if(length(peak) > 1)
        ratio = abs(peak(1,2)/peak(1,1));
        peak = peak(:, peak(2,:)<10 | peak(2,:)>22);
        if(ratio > OL_DOUBLE_THRESH)
            inv_peak = [];
            % Find the two minima
            Xinv = 1.01*max(X(i,:)) - X(i,:);
            [inv_peak(1,:), inv_peak(2,:)] = findpeaks(Xinv);
            R_min = inv_peak(2,find(inv_peak(2,:)>peak(2,1), 1));
            L_min = inv_peak(2,find(inv_peak(2,:)<peak(2,1), 1, 'last'));
            if(isempty(L_min)), L_min = 1; end
            if(isempty(R_min)), R_min = size(X,2); end
            
            % Interpolate and supress the double Spike_Snips_1
            a = X(i,L_min);
            b = X(i,R_min);
            if(a < b)
            Sm = a:(abs(b-a)/abs(R_min-L_min)):b;
            else
            Sm = a:-(abs(b-a)/abs(R_min-L_min)):b;
            end
            X_Sm(i,L_min:R_min) = Sm;
        end
    end
end

%%% Amplification using double gaussian window
g_vect_1 = normpdf(1:48, 13, 4);
g_vect_1 = g_vect_1 * ((FACTOR_AMP-1)/max(g_vect_1));
g_vect_2 = normpdf(1:48, 25, 4);
g_vect_2 = g_vect_2 * ((FACTOR_AMP-1)/max(g_vect_2));
Amp_Fact = 1 + g_vect_1 + g_vect_2;
X_amp = (repmat(Amp_Fact, size(X_Sm,1), 1) .* X_Sm);

%%%
%%% 2. Feature Extraction
%%% Wavelet transform
Wavelt_Input = X_amp;
Wavelt_range = 1:size(Wavelt_Input,2);
Wavelet_Coeff = zeros(size(Wavelt_Input,1), length(Wavelt_range));
for i = 1:size(Wavelt_Input,1)
    %Wavelet_Coeff(i,:) = wavedec(Spike_Snips(i,range), WAV_LEVEL, 'haar');
    Wavelet_Coeff(i,:) = mdwt(Wavelt_Input(i,Wavelt_range), daubcqf(4,'miv'), WAV_LEVEL);
end

%%%
%%% PCA
PCA_feat_range = 24:48; % Use only the high frequency band
PCA_in = Wavelet_Coeff(~Outliers_Mask, PCA_feat_range);
[PCA_Coeff, PCA_Score, PCA_latent, PCA_t, PCS_e, PCA_mu] = pca( PCA_in );

X_PCA = PCA_Score(:,1:3);
X_all = Wavelet_Coeff(:,PCA_feat_range);
X_PCA_Test =  X_all - repmat(mean(X_all), [size(X_all,1) 1]);
X_PCA_Test = X_PCA_Test * PCA_Coeff;

%%%
%%% 4. Clustering
%%% Hierarchical clustering
DIST = 'cityblock';
%DIST = 'mahalanobis';
Y_Pure = clusterdata(X_PCA, 'linkage', 'ward', 'maxclust', k,...
                    'distance', DIST);
X_Pure = X_PCA;

%%%
%%% 5. Fit outliers to clusters
KNN_Model = fitcknn(X_Pure, Y_Pure, 'NumNeighbors', 7);
Y_Test = predict(KNN_Model, X_PCA_Test(:,1:3));


%%%%%%%%%%%% Ploting of Results %%%%%%%%%%%%
%%%
PlotColor =      ['r', 'g', 'b', 'm', 'k', 'c', 'y'];
PlotColor_mean = ['k', 'k', 'y', 'k', 'w', 'k', 'k'];
leg_text = cell(k,1);
%%% Plot Test Results Alone (Training Results are not plotted)
for i = 1:k
    C = Spikes_Test_2((Y_Test==i), :);
    figure(2); subplot(2,2,i);
    plot(C', PlotColor(i));
    hold on;
    plot(mean(C), PlotColor_mean(i));
    title(['Test Spike Cluster # ' num2str(i)]);
    subplot(2,2,4);
    plot(C', PlotColor(i));
    hold on;
    figure(5); subplot(2,2,DS_Num);
    C = X_PCA_Test((Y_Test==i), :);
    scatter3(C(:,1), C(:,2), C(:,3), PlotColor(i));
    hold on;
end
figure(2);
subplot(2,2,4);
title('Overlaped Clustered Test Spikes');
figure(5);
subplot(2,2,DS_Num);
title('Test Dataset #2: PCA Space');
xlabel('Wavelet Coeff 1');
ylabel('Wavelet Coeff 2');
zlabel('Wavelet Coeff 3');
hold off;

xlswrite('Labels_Sampledata_test_2.xlsx', Y_Test);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%% Model 3: Dataset 3 %%%%%%%%%%%%%%%%%%%%%
%%%
%%% Design Parameters
clearvars -except Spikes_Train_1 Spikes_Train_2 ...
                  Spikes_Train_3 Spikes_Train_4 ...
                  Spikes_Test_1 Spikes_Test_2 ...
                  Spikes_Test_3 Spikes_Test_4
WAV_LEVEL = 4;
FACTOR_AMP = 5;
OUTLIER_SD = 2;
OL_DOUBLE_THRESH = 0.30;
DS_Num = 3;
RESCALE_SIZE_X = 75;
PCA_DIM = 3;

%%%
%%% Spike snipets
LenSnips = size(Spikes_Train_3, 2);
NumSnips = size(Spikes_Train_3, 1);

fprintf('\nDataset #3: Training...\n');
%%%%%%%%%%%% Training Dataset #3 %%%%%%%%%%%%
%%%
%%% 1. Pre-Processing
%%% Remove outliers
Outliers_bound = mean(Spikes_Train_3) + abs(OUTLIER_SD .* std(Spikes_Train_3));
Outliers_Mask = zeros(NumSnips, 1);
for i=1:size(Spikes_Train_3,1)
    comparison = Spikes_Train_3(i,:) >= Outliers_bound;
    if ( sum(comparison) > 0)
        Outliers_Mask(i,:) = 1;
    end
end
Spikes_No_Outliers = Spikes_Train_3(~Outliers_Mask, :);

%%%
%%% Remove Double Spikes
X = Spikes_Train_3;
X_Sm = X;
for i = 1:size(X,1)
    % Find the peaks
    peak = [];
    [peak(1,:), peak(2,:)] = findpeaks(X(i,:));
    peak = [peak [X(i,1) X(i,end); 1 size(X,2)]];
    [~,sort_ind] = sort(peak(1,:), 'descend');
    peak = peak(:, sort_ind);
    
    % Check for couble Spike_Snips_1
    if(length(peak) > 1)
        ratio = abs(peak(1,2)/peak(1,1));
        peak = peak(:, peak(2,:)<10 | peak(2,:)>22);
        if(ratio > OL_DOUBLE_THRESH)
            inv_peak = [];
            % Find the two minima
            Xinv = 1.01*max(X(i,:)) - X(i,:);
            [inv_peak(1,:), inv_peak(2,:)] = findpeaks(Xinv);
            R_min = inv_peak(2,find(inv_peak(2,:)>peak(2,1), 1));
            L_min = inv_peak(2,find(inv_peak(2,:)<peak(2,1), 1, 'last'));
            if(isempty(L_min)), L_min = 1; end
            if(isempty(R_min)), R_min = size(X,2); end
            
            % Interpolate and supress the double Spike_Snips_1
            a = X(i,L_min);
            b = X(i,R_min);
            if(a < b)
            Sm = a:(abs(b-a)/abs(R_min-L_min)):b;
            else
            Sm = a:-(abs(b-a)/abs(R_min-L_min)):b;
            end
            X_Sm(i,L_min:R_min) = Sm;
        end
    end
end

%%% Amplification using double gaussian window
g_vect_1 = normpdf(1:48, 13, 4);
g_vect_1 = g_vect_1 * ((FACTOR_AMP-1)/max(g_vect_1));
g_vect_2 = normpdf(1:48, 25, 4);
g_vect_2 = g_vect_2 * ((FACTOR_AMP-1)/max(g_vect_2));
Amp_Fact = 1 + g_vect_1 + g_vect_2;
X_amp = (repmat(Amp_Fact, size(X_Sm,1), 1) .* X_Sm);

%%%
%%% 2. Feature Extraction
%%% Wavelet transform
Wavelt_Input = X_amp;
Wavelt_range = 1:size(Wavelt_Input,2);
Wavelet_Coeff = zeros(size(Wavelt_Input,1), length(Wavelt_range));
for i = 1:size(Wavelt_Input,1)
    %Wavelet_Coeff(i,:) = wavedec(Spike_Snips(i,range), WAV_LEVEL, 'haar');
    Wavelet_Coeff(i,:) = mdwt(Wavelt_Input(i,Wavelt_range), daubcqf(4,'miv'), WAV_LEVEL);
end

%%%
%%% PCA
PCA_feat_range = 24:48; % Use only the high frequency band
PCA_in = Wavelet_Coeff(~Outliers_Mask, PCA_feat_range);
[PCA_Coeff, PCA_Score, PCA_latent, PCA_t, PCS_e, PCA_mu] = pca( PCA_in );

X_PCA = PCA_Score(:,1:3);
X_all = Wavelet_Coeff(:,PCA_feat_range);
X_PCA_Train =  X_all - repmat(mean(X_all), [size(X_all,1) 1]);
X_PCA_Train = X_PCA_Train * PCA_Coeff;

%%%
%%% 4. Clustering
%%% Find the Number of clusters using Vision based technique
nk = nchoosek(1:PCA_DIM,2);
NumClus_vi = [];
for vi = 1:PCA_DIM
    fig = figure(12);
    set(fig, 'Visible', 'off');
    scatter(PCA_Score(:,nk(vi,1)), PCA_Score(:,nk(vi,2)));
    F = getframe(fig);
    close(fig);
    
    img_rgb = F.cdata;
    RESCALE_SIZE_Y = size(img_rgb,2)/size(img_rgb,1)*RESCALE_SIZE_X;
    img_rgb = imresize(img_rgb, [RESCALE_SIZE_X RESCALE_SIZE_Y]);
    img_gray = rgb2gray(img_rgb);
    img_gray = imfilter(img_gray, fspecial('gaussian',[5 5], 2), 'replicate');
    img_bin = ~im2bw(img_gray, graythresh(img_gray));
    img_bin = imopen(img_bin, strel('disk', 2));
    img_stat = regionprops(img_bin, 'Centroid');
    NumClus_vi = [NumClus_vi, size(img_stat, 1);];
end
k = max(NumClus_vi);
fprintf('Number of Clusters Found: %d\n', k);

%%% Hierarchical clustering
%DIST = 'cityblock';
DIST = 'mahalanobis';
Y_Pure = clusterdata(X_PCA, 'linkage', 'ward', 'maxclust', k,...
                    'distance', DIST);
X_Pure = X_PCA;

%%%
%%% 5. Fit outliers to clusters
KNN_Model = fitcknn(X_Pure, Y_Pure, 'NumNeighbors', 7);
Y_Train = predict(KNN_Model, X_PCA_Train(:,1:3));


fprintf('Dataset #3: Testing...\n');
%%%%%%%%%%%% Testing Dataset #3 %%%%%%%%%%%%
%%%
%%% 1. Pre-Processing
%%% Remove outliers
Outliers_bound = mean(Spikes_Test_3) + abs(OUTLIER_SD .* std(Spikes_Test_3));
Outliers_Mask = zeros(NumSnips, 1);
for i=1:size(Spikes_Test_3,1)
    comparison = Spikes_Test_3(i,:) >= Outliers_bound;
    if ( sum(comparison) > 0)
        Outliers_Mask(i,:) = 1;
    end
end
Spikes_No_Outliers = Spikes_Test_3(~Outliers_Mask, :);

%%%
%%% Remove Double Spikes
X = Spikes_Test_3;
X_Sm = X;
for i = 1:size(X,1)
    % Find the peaks
    peak = [];
    [peak(1,:), peak(2,:)] = findpeaks(X(i,:));
    peak = [peak [X(i,1) X(i,end); 1 size(X,2)]];
    [~,sort_ind] = sort(peak(1,:), 'descend');
    peak = peak(:, sort_ind);
    
    % Check for couble Spike_Snips_1
    if(length(peak) > 1)
        ratio = abs(peak(1,2)/peak(1,1));
        peak = peak(:, peak(2,:)<10 | peak(2,:)>22);
        if(ratio > OL_DOUBLE_THRESH)
            inv_peak = [];
            % Find the two minima
            Xinv = 1.01*max(X(i,:)) - X(i,:);
            [inv_peak(1,:), inv_peak(2,:)] = findpeaks(Xinv);
            R_min = inv_peak(2,find(inv_peak(2,:)>peak(2,1), 1));
            L_min = inv_peak(2,find(inv_peak(2,:)<peak(2,1), 1, 'last'));
            if(isempty(L_min)), L_min = 1; end
            if(isempty(R_min)), R_min = size(X,2); end
            
            % Interpolate and supress the double Spike_Snips_1
            a = X(i,L_min);
            b = X(i,R_min);
            if(a < b)
            Sm = a:(abs(b-a)/abs(R_min-L_min)):b;
            else
            Sm = a:-(abs(b-a)/abs(R_min-L_min)):b;
            end
            X_Sm(i,L_min:R_min) = Sm;
        end
    end
end

%%% Amplification using double gaussian window
g_vect_1 = normpdf(1:48, 13, 4);
g_vect_1 = g_vect_1 * ((FACTOR_AMP-1)/max(g_vect_1));
g_vect_2 = normpdf(1:48, 25, 4);
g_vect_2 = g_vect_2 * ((FACTOR_AMP-1)/max(g_vect_2));
Amp_Fact = 1 + g_vect_1 + g_vect_2;
X_amp = (repmat(Amp_Fact, size(X_Sm,1), 1) .* X_Sm);

%%%
%%% 2. Feature Extraction
%%% Wavelet transform
Wavelt_Input = X_amp;
Wavelt_range = 1:size(Wavelt_Input,2);
Wavelet_Coeff = zeros(size(Wavelt_Input,1), length(Wavelt_range));
for i = 1:size(Wavelt_Input,1)
    %Wavelet_Coeff(i,:) = wavedec(Spike_Snips(i,range), WAV_LEVEL, 'haar');
    Wavelet_Coeff(i,:) = mdwt(Wavelt_Input(i,Wavelt_range), daubcqf(4,'miv'), WAV_LEVEL);
end

%%%
%%% PCA
PCA_feat_range = 24:48; % Use only the high frequency band
PCA_in = Wavelet_Coeff(~Outliers_Mask, PCA_feat_range);
[PCA_Coeff, PCA_Score, PCA_latent, PCA_t, PCS_e, PCA_mu] = pca( PCA_in );

X_PCA = PCA_Score(:,1:3);
X_all = Wavelet_Coeff(:,PCA_feat_range);
X_PCA_Test =  X_all - repmat(mean(X_all), [size(X_all,1) 1]);
X_PCA_Test = X_PCA_Test * PCA_Coeff;

%%%
%%% 4. Clustering
%%% Hierarchical clustering
%DIST = 'cityblock';
DIST = 'mahalanobis';
Y_Pure = clusterdata(X_PCA, 'linkage', 'ward', 'maxclust', k,...
                    'distance', DIST);
X_Pure = X_PCA;

%%%
%%% 5. Fit outliers to clusters
KNN_Model = fitcknn(X_Pure, Y_Pure, 'NumNeighbors', 7);
Y_Test = predict(KNN_Model, X_PCA_Test(:,1:3));


%%%%%%%%%%%% Ploting of Results %%%%%%%%%%%%
%%%
PlotColor =      ['r', 'g', 'b', 'm', 'k', 'c', 'y'];
PlotColor_mean = ['k', 'k', 'y', 'k', 'w', 'k', 'k'];
leg_text = cell(k,1);
%%% Plot Test Results Alone (Training Results are not plotted)
for i = 1:k
    C = Spikes_Test_3((Y_Test==i), :);
    figure(3); subplot(2,2,i);
    plot(C', PlotColor(i));
    hold on;
    plot(mean(C), PlotColor_mean(i));
    title(['Test Spike Cluster # ' num2str(i)]);
    subplot(2,2,4);
    plot(C', PlotColor(i));
    hold on;
    figure(5); subplot(2,2,DS_Num);
    C = X_PCA_Test((Y_Test==i), :);
    scatter3(C(:,1), C(:,2), C(:,3), PlotColor(i));
    hold on;
end
figure(3);
subplot(2,2,4);
title('Overlaped Clustered Test Spikes');
figure(5);
subplot(2,2,DS_Num);
title('Test Dataset #3: PCA Space');
xlabel('Wavelet Coeff 1');
ylabel('Wavelet Coeff 2');
zlabel('Wavelet Coeff 3');
hold off;

xlswrite('Labels_Sampledata_test_3.xlsx', Y_Test);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%% Model 4: Dataset 4 %%%%%%%%%%%%%%%%%%%%%
%%%
%%% Design Parameters
clearvars -except Spikes_Train_1 Spikes_Train_2 ...
                  Spikes_Train_3 Spikes_Train_4 ...
                  Spikes_Test_1 Spikes_Test_2 ...
                  Spikes_Test_3 Spikes_Test_4
WAV_LEVEL = 4;
FACTOR_AMP = 5;
OUTLIER_SD = 2;
OL_DOUBLE_THRESH = 0.30;
DS_Num = 4;
RESCALE_SIZE_X = 100;
PCA_DIM = 3;

%%%
%%% Spike snipets
LenSnips = size(Spikes_Train_4, 2);
NumSnips = size(Spikes_Train_4, 1);

fprintf('\nDataset #4: Training...\n');
%%%%%%%%%%%% Training Dataset #4 %%%%%%%%%%%%
%%%
%%% 1. Pre-Processing
%%% Remove outliers
Outliers_bound = mean(Spikes_Train_4) + abs(OUTLIER_SD .* std(Spikes_Train_4));
Outliers_Mask = zeros(NumSnips, 1);
for i=1:size(Spikes_Train_4,1)
    comparison = Spikes_Train_4(i,:) >= Outliers_bound;
    if ( sum(comparison) > 0)
        Outliers_Mask(i,:) = 1;
    end
end
Spikes_No_Outliers = Spikes_Train_4(~Outliers_Mask, :);

%%%
%%% Remove Double Spikes
X = Spikes_Train_4;
X_Sm = X;
for i = 1:size(X,1)
    % Find the peaks
    peak = [];
    [peak(1,:), peak(2,:)] = findpeaks(X(i,:));
    peak = [peak [X(i,1) X(i,end); 1 size(X,2)]];
    [~,sort_ind] = sort(peak(1,:), 'descend');
    peak = peak(:, sort_ind);
    
    % Check for couble Spike_Snips_1
    if(length(peak) > 1)
        ratio = abs(peak(1,2)/peak(1,1));
        peak = peak(:, peak(2,:)<10 | peak(2,:)>22);
        if(ratio > OL_DOUBLE_THRESH)
            inv_peak = [];
            % Find the two minima
            Xinv = 1.01*max(X(i,:)) - X(i,:);
            [inv_peak(1,:), inv_peak(2,:)] = findpeaks(Xinv);
            R_min = inv_peak(2,find(inv_peak(2,:)>peak(2,1), 1));
            L_min = inv_peak(2,find(inv_peak(2,:)<peak(2,1), 1, 'last'));
            if(isempty(L_min)), L_min = 1; end
            if(isempty(R_min)), R_min = size(X,2); end
            
            % Interpolate and supress the double Spike_Snips_1
            a = X(i,L_min);
            b = X(i,R_min);
            if(a < b)
            Sm = a:(abs(b-a)/abs(R_min-L_min)):b;
            else
            Sm = a:-(abs(b-a)/abs(R_min-L_min)):b;
            end
            X_Sm(i,L_min:R_min) = Sm;
        end
    end
end

%%% Amplification using double gaussian window
g_vect_1 = normpdf(1:48, 13, 4);
g_vect_1 = g_vect_1 * ((FACTOR_AMP-1)/max(g_vect_1));
g_vect_2 = normpdf(1:48, 25, 4);
g_vect_2 = g_vect_2 * ((FACTOR_AMP-1)/max(g_vect_2));
Amp_Fact = 1 + g_vect_1 + g_vect_2;
X_amp = (repmat(Amp_Fact, size(X_Sm,1), 1) .* X_Sm);

%%%
%%% 2. Feature Extraction
%%% Wavelet transform
Wavelt_Input = X_amp;
Wavelt_range = 1:size(Wavelt_Input,2);
Wavelet_Coeff = zeros(size(Wavelt_Input,1), length(Wavelt_range));
for i = 1:size(Wavelt_Input,1)
    %Wavelet_Coeff(i,:) = wavedec(Spike_Snips(i,range), WAV_LEVEL, 'haar');
    Wavelet_Coeff(i,:) = mdwt(Wavelt_Input(i,Wavelt_range), daubcqf(4,'miv'), WAV_LEVEL);
end

%%%
%%% PCA
PCA_feat_range = 24:48; % Use only the high frequency band
PCA_in = Wavelet_Coeff(~Outliers_Mask, PCA_feat_range);
[PCA_Coeff, PCA_Score, PCA_latent, PCA_t, PCS_e, PCA_mu] = pca( PCA_in );

X_PCA = PCA_Score(:,1:3);
X_all = Wavelet_Coeff(:,PCA_feat_range);
X_PCA_Train =  X_all - repmat(mean(X_all), [size(X_all,1) 1]);
X_PCA_Train = X_PCA_Train * PCA_Coeff;

%%%
%%% 4. Clustering
%%% Find the Number of clusters using Vision based technique
nk = nchoosek(1:PCA_DIM,2);
NumClus_vi = [];
for vi = 1:PCA_DIM
    fig = figure(12);
    set(fig, 'Visible', 'off');
    scatter(PCA_Score(:,nk(vi,1)), PCA_Score(:,nk(vi,2)));
    F = getframe(fig);
    close(fig);
    
    img_rgb = F.cdata;
    RESCALE_SIZE_Y = size(img_rgb,2)/size(img_rgb,1)*RESCALE_SIZE_X;
    img_rgb = imresize(img_rgb, [RESCALE_SIZE_X RESCALE_SIZE_Y]);
    img_gray = rgb2gray(img_rgb);
    img_gray = imfilter(img_gray, fspecial('gaussian',[5 5], 2), 'replicate');
    img_bin = ~im2bw(img_gray, graythresh(img_gray));
    img_bin = imopen(img_bin, strel('disk', 2));
    img_stat = regionprops(img_bin, 'Centroid');
    NumClus_vi = [NumClus_vi, size(img_stat, 1);];
end
k = max(NumClus_vi);
fprintf('Number of Clusters Found: %d\n', k);

%%% Hierarchical clustering
DIST = 'cityblock';
%DIST = 'mahalanobis';
Y_Pure = clusterdata(X_PCA, 'linkage', 'ward', 'maxclust', k+1,...
                    'distance', DIST);
X_Pure = X_PCA;

C_mean = [];
combine_class = [];
for i = 1:k+1
    C = X_Pure((Y_Pure==i), :);
    C_mean = [C_mean; mean(C)];
end
p = nchoosek(1:k+1, 2);
diff_min = inf;
for i = 1:size(p,1)
    diff = pdist2(C_mean(p(i,1),:), C_mean(p(i,2),:), 'cosine');
    if((diff < diff_min))
        diff_min = diff;
        combine_class = p(i,:);
    end
end
Y_Pure(Y_Pure==combine_class(2)) = combine_class(1);
Y_Pure(Y_Pure==k+1) = combine_class(2);

%%%
%%% 5. Fit outliers to clusters
KNN_Model = fitcknn(X_Pure, Y_Pure, 'NumNeighbors', 7);
Y_Train = predict(KNN_Model, X_PCA_Train(:,1:3));


fprintf('Dataset #4: Testing...\n');
%%%%%%%%%%%% Testing Dataset #4 %%%%%%%%%%%%
%%%
%%% 1. Pre-Processing
%%% Remove outliers
Outliers_bound = mean(Spikes_Test_4) + abs(OUTLIER_SD .* std(Spikes_Test_4));
Outliers_Mask = zeros(NumSnips, 1);
for i=1:size(Spikes_Test_4,1)
    comparison = Spikes_Test_4(i,:) >= Outliers_bound;
    if ( sum(comparison) > 0)
        Outliers_Mask(i,:) = 1;
    end
end
Spikes_No_Outliers = Spikes_Test_4(~Outliers_Mask, :);

%%%
%%% Remove Double Spikes
X = Spikes_Test_4;
X_Sm = X;
for i = 1:size(X,1)
    % Find the peaks
    peak = [];
    [peak(1,:), peak(2,:)] = findpeaks(X(i,:));
    peak = [peak [X(i,1) X(i,end); 1 size(X,2)]];
    [~,sort_ind] = sort(peak(1,:), 'descend');
    peak = peak(:, sort_ind);
    
    % Check for couble Spike_Snips_1
    if(length(peak) > 1)
        ratio = abs(peak(1,2)/peak(1,1));
        peak = peak(:, peak(2,:)<10 | peak(2,:)>22);
        if(ratio > OL_DOUBLE_THRESH)
            inv_peak = [];
            % Find the two minima
            Xinv = 1.01*max(X(i,:)) - X(i,:);
            [inv_peak(1,:), inv_peak(2,:)] = findpeaks(Xinv);
            R_min = inv_peak(2,find(inv_peak(2,:)>peak(2,1), 1));
            L_min = inv_peak(2,find(inv_peak(2,:)<peak(2,1), 1, 'last'));
            if(isempty(L_min)), L_min = 1; end
            if(isempty(R_min)), R_min = size(X,2); end
            
            % Interpolate and supress the double Spike_Snips_1
            a = X(i,L_min);
            b = X(i,R_min);
            if(a < b)
            Sm = a:(abs(b-a)/abs(R_min-L_min)):b;
            else
            Sm = a:-(abs(b-a)/abs(R_min-L_min)):b;
            end
            X_Sm(i,L_min:R_min) = Sm;
        end
    end
end

%%% Amplification using double gaussian window
g_vect_1 = normpdf(1:48, 13, 4);
g_vect_1 = g_vect_1 * ((FACTOR_AMP-1)/max(g_vect_1));
g_vect_2 = normpdf(1:48, 25, 4);
g_vect_2 = g_vect_2 * ((FACTOR_AMP-1)/max(g_vect_2));
Amp_Fact = 1 + g_vect_1 + g_vect_2;
X_amp = (repmat(Amp_Fact, size(X_Sm,1), 1) .* X_Sm);

%%%
%%% 2. Feature Extraction
%%% Wavelet transform
Wavelt_Input = X_amp;
Wavelt_range = 1:size(Wavelt_Input,2);
Wavelet_Coeff = zeros(size(Wavelt_Input,1), length(Wavelt_range));
for i = 1:size(Wavelt_Input,1)
    %Wavelet_Coeff(i,:) = wavedec(Spike_Snips(i,range), WAV_LEVEL, 'haar');
    Wavelet_Coeff(i,:) = mdwt(Wavelt_Input(i,Wavelt_range), daubcqf(4,'miv'), WAV_LEVEL);
end

%%%
%%% PCA
PCA_feat_range = 24:48; % Use only the high frequency band
PCA_in = Wavelet_Coeff(~Outliers_Mask, PCA_feat_range);
[PCA_Coeff, PCA_Score, PCA_latent, PCA_t, PCS_e, PCA_mu] = pca( PCA_in );

X_PCA = PCA_Score(:,1:3);
X_all = Wavelet_Coeff(:,PCA_feat_range);
X_PCA_Test =  X_all - repmat(mean(X_all), [size(X_all,1) 1]);
X_PCA_Test = X_PCA_Test * PCA_Coeff;

%%%
%%% 4. Clustering
%%% Hierarchical clustering
DIST = 'cityblock';
%DIST = 'mahalanobis';
Y_Pure = clusterdata(X_PCA, 'linkage', 'ward', 'maxclust', k+1,...
                    'distance', DIST);
X_Pure = X_PCA;

C_mean = [];
combine_class = [];
for i = 1:k+1
    C = X_Pure((Y_Pure==i), :);
    C_mean = [C_mean; mean(C)];
end
p = nchoosek(1:k+1, 2);
diff_min = inf;
for i = 1:size(p,1)
    diff = pdist2(C_mean(p(i,1),:), C_mean(p(i,2),:), 'cosine');
    if((diff < diff_min))
        diff_min = diff;
        combine_class = p(i,:);
    end
end
Y_Pure(Y_Pure==combine_class(2)) = combine_class(1);
Y_Pure(Y_Pure==k+1) = combine_class(2);

%%%
%%% 5. Fit outliers to clusters
KNN_Model = fitcknn(X_Pure, Y_Pure, 'NumNeighbors', 7);
Y_Test = predict(KNN_Model, X_PCA_Test(:,1:3));


%%%%%%%%%%%% Ploting of Results %%%%%%%%%%%%
%%%
PlotColor =      ['r', 'g', 'b', 'm', 'k', 'c', 'y'];
PlotColor_mean = ['k', 'k', 'y', 'k', 'w', 'k', 'k'];
leg_text = cell(k,1);
%%% Plot Test Results Alone (Training Results are not plotted)
for i = 1:k+1
    C = Spikes_Test_4((Y_Test==i), :);
    figure(4); subplot(2,2,i);
    plot(C', PlotColor(i));
    hold on;
    plot(mean(C), PlotColor_mean(i));
    title(['Test Spike Cluster # ' num2str(i)]);
    subplot(2,2,4);
    plot(C', PlotColor(i));
    hold on;
    figure(5); subplot(2,2,DS_Num);
    C = X_PCA_Test((Y_Test==i), :);
    scatter3(C(:,1), C(:,2), C(:,3), PlotColor(i));
    hold on;
end
figure(4);
subplot(2,2,4);
title('Overlaped Clustered Test Spikes');
figure(5);
subplot(2,2,DS_Num);
title('Test Dataset #4: PCA Space');
xlabel('Wavelet Coeff 1');
ylabel('Wavelet Coeff 2');
zlabel('Wavelet Coeff 3');
hold off;

xlswrite('Labels_Sampledata_test_4.xlsx', Y_Test);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End of File %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
