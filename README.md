# Automatic Neural Spike Classification

## Problem Description
According to Scholarpedia (http://www.scholarpedia.org/article/Spike_sorting), spike sorting is the grouping of spikes into clusters based on the similarity of their shapes. Given that, in principle, each neuron tends to fire spikes of a particular shape, the resulting clusters correspond to the activity of different putative neurons. The end result of spike sorting is the determination of which spike corresponds to which of these neurons.

A large amount of research in Neuroscience is based on the study of the activity of neurons recorded extracellularly with very thin electrodes implanted in animals’ brains. These microwires ‘listen’ to a few neurons close-by the electrode tip that fire action potentials or ‘spikes’. Each neuron has spikes of a characteristic shape, which is mainly determined by the morphology of their dendritic trees and the distance and orientation relative to the recording electrode (Gold et al., 2006).

Spike sorting –i.e. the classification of which spike corresponds to which neuron- is a very challenging problem. Then, before tackling mathematical details and technical issues, it is important to discuss why we need to do such a job, rather than just detecting the spikes for each channel without caring from which neuron they come.

## Project
In this project,we sort four real neural spike data sets using various techniques such as wavelet coefficients and hierarchical clustering.

There are various challenges in analyzing the real data. One of the challenges is that as time passes , the recordings from the same electrode from the same neurons will vary in spike shape and amplitude. In addition, overlap between two different neurons might occur. Thirdly, noise can be introduced in the dataset which needs to be filtered out. In the dataset given, some of these problems are already addressed while some, such as overlap between two different neurons are s till present. Before proceeding with the sorting process , such data has to be cleaned. The preprocessing techniques employed for this project are given in the forthcoming sections .

Data preprocessing techniques such as selective amplification and outliers' removal were performed to improve the model performance in terms of accuracy.Furthermore, we automated the development of the model by employing vision-based techniques. We then compared and contrasted the accuracy of our models on the training and test datasets.
